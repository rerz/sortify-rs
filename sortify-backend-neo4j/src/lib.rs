#![feature(format_args_capture)]

use futures::{AsyncRead, AsyncWrite};
use itertools::Itertools;
use neo4rs::types::{BoltNode, BoltRelation, BoltString};
use neo4rs::{types::BoltList, BoltType};
use serde::{Deserialize, Serialize};
use sortify_graph_traits::*;
use std::collections::hash_map::RandomState;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::env;
use std::fmt::Debug;
use std::iter::FromIterator;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::ops::{Deref, DerefMut};
use tokio::io::BufStream;

pub type NeoGraph = neo4rs::Graph;

#[derive(Serialize, Deserialize, Debug)]
pub struct NeoNode<N: Node>(pub i64, pub Vec<String>, pub N);

impl<N: Node> NeoNode<N> {
    pub fn id(&self) -> i64 {
        self.0
    }

    pub fn labels(&self) -> &[String] {
        &self.1
    }

    pub fn node(&self) -> &N {
        &self.2
    }

    pub fn node_mut(&mut self) -> &mut N {
        &mut self.2
    }
}

impl<N: Node> Deref for NeoNode<N> {
    type Target = N;

    fn deref(&self) -> &Self::Target {
        self.node()
    }
}

impl<N: Node> DerefMut for NeoNode<N> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.node_mut()
    }
}

fn get_node_labels<N: Node>() -> String {
    format!(
        "{}",
        N::get_labels().into_iter().collect::<Vec<_>>().join(":")
    )
}

fn get_edge_labels<E: Edge>() -> String {
    format!(
        "{}",
        E::get_labels().into_iter().collect::<Vec<_>>().join(":")
    )
}

pub async fn get_graph(uri: &str, user: &str, password: &str) -> Result<NeoGraph, anyhow::Error> {
    NeoGraph::new(uri, user, password)
        .await
        .map_err(|_| anyhow::anyhow!("Failed to connect to graph"))
}

pub async fn find_by_id<N: Node>(graph: &NeoGraph, id: i64) -> Result<NeoNode<N>, anyhow::Error> {
    let labels_string = get_node_labels::<N>();

    let stmt = format!("MATCH (n:{labels_string}) WHERE id(n) = $id RETURN n;");

    let mut result = graph
        .execute(neo4rs::query(&stmt).param("id", id))
        .await
        .map_err(|_| anyhow::anyhow!("Failed to execute query"))?; //;

    let row = result
        .next()
        .await
        .map_err(|_| anyhow::anyhow!("Failed to get next row"))?;

    let node = row
        .unwrap()
        .get::<neo4rs::Node>("n")
        .ok_or(anyhow::anyhow!("Failed to find node"))?;

    Ok(NeoNode(node.id(), node.labels(), N::from_bolt_node(&node)))
}

pub async fn query<
    'a,
    T: TryFrom<BoltType>,
    B: Into<BoltType> + Debug,
    I: IntoIterator<Item = (&'a str, B)>,
>(
    graph: &NeoGraph,
    query: &'a str,
    params: I,
    result_name: &str,
) -> Result<T, anyhow::Error> {
    let mut query = neo4rs::query(dbg!(query));

    for (key, value) in params {
        dbg!(&value);
        query = query.param(key, value);
    }

    let mut result = graph
        .execute(query)
        .await
        .map_err(|_| anyhow::anyhow!("Failed to execute query"))?;
    let row = result
        .next()
        .await
        .map_err(|_| anyhow::anyhow!("Failed to get row"))?;

    Ok(row
        .unwrap()
        .get::<T>(result_name)
        .ok_or(anyhow::anyhow!("Failed to convert type"))?)
}

pub async fn query_subgraph<'a, B: Into<BoltType> + Debug, I: IntoIterator<Item = (&'a str, B)>>(
    graph: &NeoGraph,
    query: &'a str,
    params: I,
) -> Result<(Vec<neo4rs::Node>, Vec<neo4rs::Relation>), anyhow::Error> {
    dbg!(&query);
    let mut query = neo4rs::query(query);

    for (key, value) in params {
        query = query.param(key, dbg!(value));
    }

    let mut result = graph
        .execute(query)
        .await
        .map_err(|_| anyhow::anyhow!("Failed to execute query"))?;
    let row = result
        .next()
        .await
        .map_err(|_| anyhow::anyhow!("Failed to get row"))?;

    let row = row.unwrap();

    let nodes = row
        .get::<neo4rs::types::BoltList>("nodes")
        .ok_or(anyhow::anyhow!("Failed to get nodes"))?
        .value
        .into_iter()
        .map(|node| neo4rs::Node::try_from(node).unwrap())
        .collect();

    let edges = row
        .get::<neo4rs::types::BoltList>("edges")
        .ok_or(anyhow::anyhow!("Failed to get edges"))?
        .value
        .into_iter()
        .map(|edge| neo4rs::Relation::try_from(edge).unwrap())
        .collect();

    Ok((nodes, edges))
}

pub async fn get_neighbors_of<N: Node, T: Into<BoltType> + Debug>(
    graph: &NeoGraph,
    (key, value): (&str, T),
) -> Result<Vec<NeoNode<N>>, anyhow::Error> {
    let stmt = format!("MATCH (n)--(m) WHERE n.{key} = $value RETURN collect(m) AS neighbors;");

    dbg!(&stmt);
    dbg!(&value);

    let mut result = graph
        .execute(neo4rs::query(&stmt).param("value", value))
        .await
        .unwrap();

    let row = result
        .next()
        .await
        .map_err(|_| anyhow::anyhow!("Failed to get next row"))?;

    let nodes = row
        .unwrap()
        .get::<BoltList>("neighbors")
        .ok_or(anyhow::anyhow!("Failed to find nodes"))?
        .value
        .into_iter()
        .map(|bolt_type| neo4rs::Node::try_from(bolt_type))
        .filter_map(Result::ok)
        .map(|node| NeoNode(node.id(), node.labels(), N::from_bolt_node(&node)));

    Ok(nodes.collect())
}

pub async fn find_by_key_value<N: Node, T: Into<BoltType>>(
    graph: &NeoGraph,
    label: Option<String>,
    (key, value): (&str, T),
    random: bool,
) -> Result<NeoNode<N>, anyhow::Error> {
    let order = if random { "ORDER BY rand()" } else { "" };

    let label = label.map(|label| format!(":{label}")).unwrap_or("".into());

    let stmt = format!("MATCH (n{label}) WHERE n.{key} = $value RETURN n {order} LIMIT 1");

    let mut result = graph
        .execute(neo4rs::query(&stmt).param("value", value))
        .await
        .unwrap();

    let row = result
        .next()
        .await
        .map_err(|_| anyhow::anyhow!("Failed to get next row"))?;

    let node = row
        .unwrap()
        .get::<neo4rs::Node>("n")
        .ok_or(anyhow::anyhow!("Failed to find node"))?;

    Ok(NeoNode(node.id(), node.labels(), N::from_bolt_node(&node)))
}

pub async fn update_node<N: Node>(
    graph: &mut NeoGraph,
    node: &NeoNode<N>,
) -> Result<NeoNode<N>, anyhow::Error> {
    let node_id = node.id();

    let properties = node
        .get_properties()
        .into_iter()
        .map(|(key, value)| (BoltString::new(&key), value));

    let stmt = format!("MATCH (n) WHERE id(n) = {node_id} SET n = $props RETURN n;");

    let map = neo4rs::types::map::BoltMap::from_iter(properties);

    let mut result = graph
        .execute(neo4rs::query(&stmt).param("props", BoltType::Map(map)))
        .await
        .map_err(|_| anyhow::anyhow!("Failed to update node"))?;

    let row = result
        .next()
        .await
        .map_err(|_| anyhow::anyhow!("Failed to get row"))?;

    let node = row
        .unwrap()
        .get::<neo4rs::Node>("n")
        .ok_or(anyhow::anyhow!("Failed to get node"))?;

    Ok(NeoNode(node.id(), node.labels(), N::from_bolt_node(&node)))
}

pub async fn create_edge<S: Node, T: Node, E: Edge<Source = S, Target = T>>(
    graph: &mut NeoGraph,
    source: &NeoNode<S>,
    target: &NeoNode<T>,
    edge: E,
) -> Result<neo4rs::Relation, anyhow::Error> {
    let source_labels = get_node_labels::<S>();
    let target_labels = get_node_labels::<T>();

    let source_id = source.id();
    let target_id = target.id();

    let edge_labels = get_edge_labels::<E>();

    let stmt = format!("MATCH (a:{source_labels}), (b:{target_labels}) WHERE ID(a) = {source_id} AND ID(b) = {target_id} MERGE (a)-[r:{edge_labels}]->(b) RETURN r");

    let mut result = graph
        .execute(neo4rs::query(&stmt))
        .await
        .map_err(|_| anyhow::anyhow!("Failed to create relation"))?;

    let row = result
        .next()
        .await
        .map_err(|_| anyhow::anyhow!("Failed to get next row"))?;

    let rel = row
        .unwrap()
        .get::<neo4rs::Relation>("r")
        .ok_or(anyhow::anyhow!("Failed to create relation"))?;

    Ok(rel)
}

pub async fn create_node_or_try_get_by_key_value<N: Node, T: Into<BoltType>>(
    graph: &mut NeoGraph,
    node: &N,
    merge: bool,
    (key, value): (&str, T),
) -> Result<NeoNode<N>, anyhow::Error> {
    match create_node(graph, node, merge).await {
        Ok(node) => Ok(node),
        Err(_) => match find_by_key_value(graph, None, (key, value), false).await {
            Ok(node) => Ok(node),
            Err(_) => Err(anyhow::anyhow!("Failed to get backup node")),
        },
    }
}

pub async fn create_node<N: Node>(
    graph: &mut NeoGraph,
    node: &N,
    merge: bool,
) -> Result<NeoNode<N>, anyhow::Error> {
    let labels_string = get_node_labels::<N>();

    let merge_or_create = if merge { "MERGE" } else { "CREATE" };

    let properties = node.get_properties();

    let attrs = properties
        .keys()
        .map(|key| format!("{key}: ${key}"))
        .collect::<Vec<String>>()
        .join(",");

    let params = properties
        .iter()
        .map(|(key, value)| (key.clone(), value.clone()))
        .collect::<Vec<_>>();

    let stmt = format!("{merge_or_create} (n:{labels_string} {{{attrs}}}) RETURN n");

    let mut query = neo4rs::query(&stmt);

    for (key, value) in params {
        query = query.param(&key, value);
    }

    let mut result = graph
        .execute(query)
        .await
        .map_err(|e| anyhow::anyhow!("Failed to execute query {:?}", e))?;

    let row = result
        .next()
        .await
        .map_err(|_| anyhow::anyhow!("Failed to get row"))?;

    let node = row
        .unwrap()
        .get::<neo4rs::Node>("n")
        .ok_or(anyhow::anyhow!("Failed to create node"))?;

    Ok(NeoNode(node.id(), node.labels(), N::from_bolt_node(&node)))
}
