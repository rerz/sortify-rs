use librespot::core::mercury;
use librespot::core::session::Session;
use librespot::core::config::SessionConfig;
use librespot::core::authentication::Credentials;
use serde::Deserialize;
use serde::Serialize;
use std::sync::Arc;
use tide::Request;

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
struct City {
    pub city: String,
    pub country: String,
    pub listeners: u64,
    pub region: String,
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
struct Image {
    pub height: u64,
    pub id: String,
    pub uri: String,
    pub width: u64,
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
struct PlaylistsObject {
    pub entries: Vec<Playlist>
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
struct Playlist {
    pub image_url: String,
    pub listeners: u64,
    pub name: String,
    pub owner: serde_json::Value,
    pub uri: String,
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
struct ArtistDetails {
    pub artist_gid: String,
    pub autobiography: serde_json::Value,
    pub cities: Vec<City>,
    pub follower_count: u64,
    pub following_count: u64,
    pub global_chart_position: u64,
    pub header_image: Image,
    pub images: Vec<Image>,
    pub images_count: u32,
    pub main_image_url: String,
    pub monthly_listeners: u64,
    pub monthly_listeners_delta: i64,
    pub playlists: PlaylistsObject
}

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    dotenv::dotenv().ok();

    println!("Hello, world!");
    let config = SessionConfig::default();
    let credentials = Credentials::with_password(dbg!(std::env::var("SPOTIFY_USER")?), dbg!(std::env::var("SPOTIFY_PASSWORD")?));

    dbg!(&credentials);

    let session = Session::connect(config, credentials, None).await?;
    let session = Arc::new(session);

    #[derive(Clone)]
    struct State {
        pub session: Arc<Session>,
    };

    let mut app = tide::with_state(State { session });

    app.at("/artist/:id/details").get(|req: Request<State>| async move {
        let session = &req.state().session;

        let resp = session.mercury().get("hm://creatorabout/v0/artist-insights/1lJhME1ZpzsEa5M0wW6Mso").await.unwrap();
        let payload = &resp.payload[0];

        let details = serde_json::from_slice::<ArtistDetails>(payload)?;

        Ok(serde_json::to_string_pretty(&details).unwrap())
    });

    app.listen("127.0.0.1:8082").await?;

    Ok(())
}
