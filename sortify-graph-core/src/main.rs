#![feature(format_args_capture)]

#[macro_use]
extern crate log;

use actix_web::web;
use actix_web::{HttpServer, Responder};
use derive_more::Display;
use serde::Serialize;
use sortify_backend_neo4j::{
    create_edge, create_node, find_by_id, find_by_key_value, NeoGraph, NeoNode,
};
use sortify_graph_traits::Node;
use sortify_graph_types::nodes::album::AlbumNode;
use sortify_graph_types::nodes::artist::ArtistNode;
use sortify_graph_types::nodes::playlist::PlaylistNode;
use sortify_graph_types::nodes::track::TrackNode;
use sortify_graph_types::nodes::SpotifyEntityNode;
use std::env;
use std::sync::Arc;
use tokio::sync::RwLock;
use serde::Deserialize;

mod error;
mod scopes;
mod services;
mod spotify;

use crate::spotify::artist::get_artist;
use actix_cors::Cors;
pub use error::Error;
use schemars::JsonSchema;
use sortify_graph_types::edges::artist_on_album::ArtistOnAlbum;

#[derive(Serialize, JsonSchema)]
pub struct NodeDTO<N: Node> {
    pub id: i64,
    pub labels: Vec<String>,
    pub node: N,
}

impl<N: Node> From<NeoNode<N>> for NodeDTO<N> {
    fn from(neo_node: NeoNode<N>) -> Self {
        Self {
            id: neo_node.0,
            labels: neo_node.1,
            node: neo_node.2,
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct UntypedNodeDTO {
    pub id: i64,
    pub labels: Vec<String>,
    pub node: serde_json::Value,
    pub community: Option<usize>,
}

impl<N: Node + Serialize> NodeDTO<N> {
    pub fn into_untyped(self) -> UntypedNodeDTO {
        UntypedNodeDTO {
            id: self.id,
            labels: self.labels,
            node: serde_json::to_value(self.node).unwrap(),
            community: None,
        }
    }
}

#[actix_web::main]
async fn main() -> Result<(), anyhow::Error> {
    dotenv::dotenv()?;
    env_logger::init();

    let graph = sortify_backend_neo4j::get_graph("localhost:7687", "neo4j", "neo").await?;

    let graph = Arc::new(RwLock::new(graph));

    let credentials = aspotify::ClientCredentials::from_env()?;
    let spotify = aspotify::Client::new(credentials);
    let spotify = Arc::new(RwLock::new(spotify));

    let server = HttpServer::new(move || {
        let cors = Cors::permissive().allow_any_origin();
        actix_web::App::new()
            .wrap(cors)
            .data(Arc::clone(&graph))
            .data(Arc::clone(&spotify))
            .service(scopes::artist::artist_scope())
            .service(scopes::album::album_scope())
            .service(scopes::track::track_scope())
            .service(scopes::playlist::playlist_scope())
            .service(scopes::expand_random_node_handler)
            .service(scopes::expand_by_id)
            .service(scopes::expand_n_levels)
            .service(scopes::query_subgraph)
    })
    .bind(format!(
        "{}:{}",
        env::var("CORE_HOST")?,
        env::var("CORE_PORT")?
    ))?
    .run()
    .await?;

    Ok(())
}

async fn add_artist_node(
    graph: &mut NeoGraph,
    spotify: &aspotify::Client,
    id: &str,
) -> Result<NeoNode<ArtistNode>, anyhow::Error> {
    let artist = spotify::artist::get_artist(spotify, id).await?;

    let artist = ArtistNode::from(artist);

    Ok(create_node(graph, &artist, true).await?)
}

async fn add_album_node(
    graph: &mut NeoGraph,
    client: &aspotify::Client,
    id: &str,
) -> Result<NeoNode<AlbumNode>, anyhow::Error> {
    // let album = reqwest::get(&format!("http://{}:{}/album/{}", env::var("CACHE_HOST")?, env::var("CACHE_PORT")?, id))
    //     .await?
    //     .json::<aspotify::Album>()
    //     .await?;
    let album = crate::spotify::album::get_album(client, id).await?;

    let album = AlbumNode::from(album);

    Ok(create_node(graph, &album, true).await?)
}

async fn add_playlist_node(
    graph: &mut NeoGraph,
    client: &aspotify::Client,
    id: &str,
) -> Result<NeoNode<PlaylistNode>, anyhow::Error> {
    // let playlist = reqwest::get(&format!("http://{}:{}/playlist/{}", env::var("CACHE_HOST")?, env::var("CACHE_PORT")?, id))
    //     .await?
    //     .json::<aspotify::Playlist>()
    //     .await?;
    let playlist = crate::spotify::playlist::get_playlist(client, id).await?;

    let playlist = PlaylistNode::from(playlist);

    Ok(create_node(graph, &playlist, true).await?)
}

async fn add_track_node(
    graph: &mut NeoGraph,
    client: &aspotify::Client,
    id: &str,
) -> Result<NeoNode<TrackNode>, anyhow::Error> {
    // let track = reqwest::get(&format!("http://{}:{}/track/{}", env::var("CACHE_HOST")?, env::var("CACHE_PORT")?, id))
    //     .await?
    //     .json::<aspotify::Track>()
    //     .await?;
    let track = crate::spotify::track::get_track(client, id).await?;

    let track = TrackNode::from(track);

    Ok(create_node(graph, &track, true).await?)
}
