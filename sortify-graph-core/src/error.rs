use derive_more::Display;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error(transparent)]
    SpotifyError(#[from] aspotify::Error),
    #[error(transparent)]
    Other(#[from] anyhow::Error)
}

impl actix_web::error::ResponseError for Error {

}



