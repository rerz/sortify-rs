pub async fn get_track(
    spotify: &aspotify::Client,
    id: &str,
) -> Result<aspotify::Track, crate::Error> {
    Ok(spotify.tracks().get_track(id, None).await?.data)
}
