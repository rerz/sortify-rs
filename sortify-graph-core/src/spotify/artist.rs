use std::env;

pub async fn get_artist(
    spotify: &aspotify::Client,
    id: &str,
) -> Result<aspotify::Artist, crate::Error> {
    Ok(spotify.artists().get_artist(id).await?.data)
}
