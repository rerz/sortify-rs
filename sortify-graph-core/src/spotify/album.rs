pub async fn get_album(
    spotify: &aspotify::Client,
    id: &str,
) -> Result<aspotify::Album, crate::Error> {
    Ok(spotify.albums().get_album(id, None).await?.data)
}
