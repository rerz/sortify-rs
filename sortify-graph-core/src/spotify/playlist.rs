pub async fn get_playlist(
    spotify: &aspotify::Client,
    id: &str,
) -> Result<aspotify::Playlist, crate::Error> {
    Ok(spotify.playlists().get_playlist(id, None).await?.data)
}
