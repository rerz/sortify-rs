use crate::{add_artist_node, NodeDTO};
use actix_web::{web, Responder, Scope};
use sortify_backend_neo4j::NeoGraph;
use std::sync::Arc;
use tokio::sync::RwLock;
use actix_web::web::Json;

pub fn artist_scope() -> Scope {
    Scope::new("artist")
        .service(expand_random_artist_node_handler)
        .service(add_artist_node_handler)
        .service(expand_artist_node_handler)
}

#[actix_web::post("{id}")]
async fn add_artist_node_handler(
    id: web::Path<String>,
    spotify: web::Data<Arc<RwLock<aspotify::Client>>>,
    graph: web::Data<Arc<RwLock<NeoGraph>>>,
) -> Result<impl Responder, crate::Error> {
    let mut graph = graph.write().await;
    let spotify = spotify.read().await;

    let artist = add_artist_node(&mut graph, &spotify, &id).await?;

    Ok(web::Json(NodeDTO::from(artist)))
}

#[actix_web::post("expand")]
async fn expand_random_artist_node_handler(spotify: web::Data<Arc<RwLock<aspotify::Client>>>,
                                           graph: web::Data<Arc<RwLock<NeoGraph>>>,) -> Result<impl Responder, crate::Error> {
    let mut graph = graph.write().await;
    let client = spotify.read().await;

    let node = crate::services::expand::expand_random_node(&mut graph, &client, Some("Artist".into())).await?;

    Ok(Json(node.into_untyped_node_dto()))

}

#[actix_web::post("{id}/expand")]
async fn expand_artist_node_handler(
    id: web::Path<String>,
    spotify: web::Data<Arc<RwLock<aspotify::Client>>>,
    graph: web::Data<Arc<RwLock<NeoGraph>>>,
) -> Result<impl Responder, crate::Error> {
    let mut graph = graph.write().await;
    let spotify = spotify.read().await;

    // add_artist_node(&mut graph, &spotify, &id).await?;

    let expanded = crate::services::expand::expand_artist_node(&mut graph, &spotify, &id).await?;

    Ok(Json(NodeDTO::from(expanded)))
}
