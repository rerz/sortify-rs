use crate::{add_album_node, NodeDTO};
use actix_web::{web, Responder, Scope};
use sortify_backend_neo4j::NeoGraph;
use std::sync::Arc;
use tokio::sync::RwLock;
use actix_web::web::Json;

pub fn album_scope() -> Scope {
    Scope::new("album")
        .service(add_album_node_handler)
        .service(expand_album_node_handler)
}

#[actix_web::post("{id}")]
async fn add_album_node_handler(
    id: web::Path<String>,
    spotify: web::Data<Arc<RwLock<aspotify::Client>>>,
    graph: web::Data<Arc<RwLock<NeoGraph>>>,
) -> Result<impl Responder, crate::Error> {
    let mut graph = graph.write().await;
    let spotify = spotify.read().await;

    let album = add_album_node(&mut graph, &spotify, &id).await?;

    Ok(web::Json(NodeDTO::from(album)))
}

#[actix_web::post("{id}/expand")]
async fn expand_album_node_handler(
    id: web::Path<String>,
    spotify: web::Data<Arc<RwLock<aspotify::Client>>>,
    graph: web::Data<Arc<RwLock<NeoGraph>>>,
) -> Result<impl Responder, crate::Error> {
    let mut graph = graph.write().await;
    let spotify = spotify.read().await;

    let expanded = crate::services::expand::expand_album_node(&mut graph, &spotify, &id).await?;

    Ok(Json(NodeDTO::from(expanded)))
}
