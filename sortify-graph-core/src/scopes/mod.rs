pub mod album;
pub mod artist;
pub mod playlist;
pub mod track;

use crate::services::expand::ExpandableNode;
use crate::{NodeDTO, UntypedNodeDTO};
use actix_web::web::Json;
use actix_web::{web, Responder};
use serde::{Deserialize, Serialize};
use sortify_backend_neo4j::{get_neighbors_of, NeoGraph};
use std::collections::{VecDeque, HashMap};
use std::sync::Arc;
use tokio::sync::RwLock;
use networky::HashMapExt;
use networky::algorithm::community::Louvain;
use networky::partition::Partition;
use networky::algorithm::coarsen::partition_coarsening;
use neo4rs::BoltType::Node;

// TODO: implement finding a suitable partition based on seed playlists

#[derive(Serialize, Deserialize)]
pub struct SubgraphDTO {
    pub nodes: Vec<UntypedNodeDTO>,
    pub edges: Vec<(i64, i64)>,
}

#[derive(Serialize, Deserialize)]
pub struct SubgraphQuery {
    pub query: String,
}

#[actix_web::get("graph")]
pub async fn query_subgraph(
    graph: web::Data<Arc<RwLock<NeoGraph>>>,
    client: web::Data<Arc<RwLock<aspotify::Client>>>,
    query_base64: web::Query<SubgraphQuery>,
) -> Result<impl Responder, crate::Error> {
    let mut graph = graph.write().await;
    let client = client.read().await;

    dbg!(&query_base64.query);

    /*    let query = base64::decode(query_base64.query).unwrap();
    let query = String::from_utf8(query).unwrap();*/

    let (nodes, edges) =
        sortify_backend_neo4j::query_subgraph::<String, _>(&graph, &query_base64.query, vec![])
            .await?;

    let mut nodes = nodes
        .into_iter()
        .map(|node: neo4rs::Node| UntypedNodeDTO { id: node.id(), labels: node.labels(), community: None, node: serde_json::json!({ "name": node.get::<String>("name").unwrap() }) })
        .collect::<Vec<_>>();

    let node_ids = nodes.iter().map(|node| node.id).collect::<Vec<_>>();

    let edges = edges
        .into_iter()
        .map(|relation: neo4rs::Relation| (relation.start_node_id(), relation.end_node_id()))
        .filter(|(u, v)| node_ids.contains(u) && node_ids.contains(v))
        .collect::<Vec<_>>();

    let mut node_mapping = HashMap::<i64, networky::node::Node>::new();

    nodes.iter().enumerate().for_each(|(i, n)| {
        node_mapping.insert(n.id, networky::node::Node(i));
    });

    let reverse_node_mapping = node_mapping.inverted();

    let mut ny_graph = networky::graph::Graph::new_with_nodes(nodes.len());
    ny_graph.add_edges(&edges.iter().map(|(u, v)| (node_mapping[u], node_mapping[v])).collect::<Vec<_>>());

    let mut louvain = Louvain {
        graph: &ny_graph,
        partition: Partition::singleton_with_capacity(ny_graph.n),
        max_iter: 100,
    };

    louvain.run();

    let (coarse, _, _) = partition_coarsening(&ny_graph, &louvain.partition);

    let mut louvain = Louvain {
        graph: &coarse,
        partition: Partition::singleton_with_capacity(coarse.n),
        max_iter: 100,
    };

    louvain.run();

    let mut coarse_edges = coarse.node_pairs().filter(|(u, v)| coarse.has_edge(*u, *v)).map(|(u, v)| (*u as i64, *v as i64)).collect::<Vec<_>>();

    let mut coarse_nodes = coarse
        .nodes()
        .filter(|u| coarse.neighbors_of(*u).count() > 0)
        .map(|u| {
            let weight = coarse.out_edges[*u].len() as f64;
            UntypedNodeDTO {
                id: *u as i64,
                community: louvain.partition[u],
                node: serde_json::json!({ "name": "community", "weight": weight }),
                labels: vec!["Community".into()],
            }
        }).collect::<Vec<_>>();

    // louvain.partition.entries().for_each(|(u, c)| if let Some(c) = c {
    //     let mut node = nodes.iter_mut().find(|n| n.id == reverse_node_mapping[&u]).unwrap();
    //     node.community = Some(c);
    // });

    Ok(Json(SubgraphDTO { nodes: coarse_nodes, edges: coarse_edges }))
}

pub async fn get_expanded_artists_subgraph(
    graph: web::Data<Arc<RwLock<NeoGraph>>>,
    client: web::Data<Arc<RwLock<aspotify::Client>>>,
) {
    let mut graph = graph.write().await;
    let client = client.read().await;

    //let (nodes, edges) = sortify_backend_neo4j::query_subgraph::<_, _>(&graph, "MATCH (n:Artist)-[r:ArtistRelatedTo]", vec![]).await?;
}

#[actix_web::post("node/expand")]
pub async fn expand_random_node_handler(
    graph: web::Data<Arc<RwLock<NeoGraph>>>,
    client: web::Data<Arc<RwLock<aspotify::Client>>>,
) -> Result<impl Responder, crate::Error> {
    let mut graph = graph.write().await;
    let client = client.read().await;

    let node = crate::services::expand::expand_random_node(&mut graph, &client, None).await?;

    Ok(Json(node.into_untyped_node_dto()))
}

#[actix_web::post("node/{id}/expand")]
pub async fn expand_by_id(
    id: web::Path<String>,
    graph: web::Data<Arc<RwLock<NeoGraph>>>,
    client: web::Data<Arc<RwLock<aspotify::Client>>>,
) -> Result<impl Responder, crate::Error> {
    let mut graph = graph.write().await;
    let client = client.read().await;

    let node = crate::services::expand::expand_node_by_id(&mut graph, &client, &id).await?;

    Ok(Json(node.into_untyped_node_dto()))
}

#[actix_web::post("node/{id}/expand/{max_depth}")]
pub async fn expand_n_levels(
    path: web::Path<(String, usize)>,
    graph: web::Data<Arc<RwLock<NeoGraph>>>,
    client: web::Data<Arc<RwLock<aspotify::Client>>>,
) -> Result<impl Responder, crate::Error> {
    let (id, max_depth) = path.into_inner();

    let mut graph = graph.write().await;
    let client = client.read().await;

    let mut to_expand = VecDeque::<String>::new();
    let mut depth = 0;

    to_expand.push_back(id);

    while !to_expand.is_empty() {
        let depth_size = to_expand.len();
        for _ in 0..depth_size {
            let node = to_expand.pop_front().unwrap();
            let node =
                crate::services::expand::expand_node_by_id(&mut graph, &client, &node).await?;
            let neighbors = get_neighbors_of::<ExpandableNode, _>(
                &mut graph,
                ("spotify_id", node.get_spotify_id()),
            )
                .await?;

            for neighbor in neighbors {
                to_expand.push_back(neighbor.get_spotify_id().into())
            }
        }
        depth += 1;

        if depth >= max_depth {
            break;
        }
    }

    Ok("")
}
