use crate::{add_track_node, NodeDTO};
use actix_web::{web, Responder, Scope};
use sortify_backend_neo4j::NeoGraph;
use std::sync::Arc;
use tokio::sync::RwLock;
use actix_web::web::Json;

pub fn track_scope() -> Scope {
    Scope::new("track").service(add_track_node_handler).service(expand_track_node_handler)
}

#[actix_web::post("{id}")]
async fn add_track_node_handler(
    id: web::Path<String>,
    spotify: web::Data<Arc<RwLock<aspotify::Client>>>,
    graph: web::Data<Arc<RwLock<NeoGraph>>>,
) -> Result<impl Responder, crate::Error> {
    let mut graph = graph.write().await;
    let spotify = spotify.read().await;

    let track_node = add_track_node(&mut graph, &spotify, &id).await?;

    Ok(web::Json(NodeDTO::from(track_node)))
}

#[actix_web::post("{id}/expand")]
async fn expand_track_node_handler(
    id: web::Path<String>,
    spotify: web::Data<Arc<RwLock<aspotify::Client>>>,
    graph: web::Data<Arc<RwLock<NeoGraph>>>,
) -> Result<impl Responder, crate::Error> {
    let mut graph = graph.write().await;
    let spotify = spotify.read().await;

    // add_artist_node(&mut graph, &spotify, &id).await?;

    let expanded = crate::services::expand::expand_track_node(&mut graph, &spotify, &id).await?;

    Ok(Json(NodeDTO::from(expanded)))
}