use actix_web::Responder;
use sortify_backend_neo4j::{create_edge, create_node, find_by_key_value, NeoGraph, update_node, NeoNode, query, create_node_or_try_get_by_key_value};
use sortify_graph_types::edges::artist_on_album::ArtistOnAlbum;
use sortify_graph_types::edges::artist_on_track::ArtistOnTrack;
use sortify_graph_types::edges::track_on_album::TrackOnAlbum;
use sortify_graph_types::edges::track_on_playlist::TrackOnPlaylist;
use sortify_graph_types::nodes::album::AlbumNode;
use sortify_graph_types::nodes::artist::ArtistNode;
use sortify_graph_types::nodes::playlist::PlaylistNode;
use sortify_graph_types::nodes::track::TrackNode;
use sortify_graph_types::edges::artist_related_to::ArtistRelatedTo;
use crate::{NodeDTO, UntypedNodeDTO};
use actix_web::web::Json;
use neo4rs::types::BoltBoolean;
use sortify_graph_traits::{Node, Expand};
use std::collections::hash_map::RandomState;
use std::collections::HashMap;
use neo4rs::BoltType;
use serde::{Serialize, Deserialize};

pub enum ExpansionStrategy {
    Single,
    Steps(usize),
}

pub async fn expand_artist_node(
    graph: &mut NeoGraph,
    client: &aspotify::Client,
    id: &str,
) -> Result<NeoNode<ArtistNode>, crate::Error> {
    let mut artist_node = find_by_key_value::<ArtistNode, _>(graph, None, ("spotify_id", id), false).await?;

    if artist_node.base_node.expanded {
        return Ok(artist_node);
    }

    let albums = client
        .artists()
        .get_artist_albums(id, None, 50, 0, None)
        .await?
        .data
        .items;

    let related = client.artists().get_related_artists(id).await?.data;

    info!("Artist {} has {} related artists", artist_node.base_node.name, related.len());

    for related_artist in related {
        let related_artist_id = related_artist.id.clone();
        let related_artist_node = create_node_or_try_get_by_key_value(graph, &ArtistNode::from(related_artist), true, ("spotify_id", related_artist_id)).await?;

        create_edge(graph, &artist_node, &related_artist_node, ArtistRelatedTo).await?;
    }

    for album in albums {
        let album_id = album.id.clone();
        let album_node = create_node_or_try_get_by_key_value(graph, &AlbumNode::from(album), true, ("spotify_id", album_id)).await?;

        create_edge(graph, &artist_node, &album_node, ArtistOnAlbum).await?;
    }

    artist_node.base_node.expanded = true;

    Ok(update_node(graph, &artist_node).await?)
}

pub async fn expand_album_node(
    graph: &mut NeoGraph,
    client: &aspotify::Client,
    id: &str,
) -> Result<NeoNode<AlbumNode>, crate::Error> {
    let mut album_node = find_by_key_value::<AlbumNode, _>(graph, None, ("spotify_id", id), false).await?;

    if album_node.base_node.expanded {
        return Ok(album_node);
    }

    let album = crate::spotify::album::get_album(client, id).await?;

    let artists = album
        .artists
        .into_iter()
        .map(|artist| ArtistNode::from(artist));

    for artist in artists {
        let artist_id = artist.base_node.spotify_id.clone();
        let artist_node = create_node_or_try_get_by_key_value(graph, &artist, true, ("spotify_id", artist_id)).await?;

        create_edge(graph, &artist_node, &album_node, ArtistOnAlbum).await?;
    }

    for track in album.tracks.items {
        let track = TrackNode::from(track);
        let track_id = track.base_node.spotify_id.clone();
        let track_node = create_node_or_try_get_by_key_value(graph, &track, true, ("spotify_id", track_id)).await?;

        create_edge(graph, &track_node, &album_node, TrackOnAlbum).await?;
    }

    album_node.base_node.expanded = true;

    Ok(update_node(graph, &album_node).await?)
}

pub async fn expand_playlist_node(
    graph: &mut NeoGraph,
    client: &aspotify::Client,
    id: &str,
) -> Result<NeoNode<PlaylistNode>, crate::Error> {
    let mut playlist_node = find_by_key_value::<PlaylistNode, _>(graph, None, ("spotify_id", id), false).await?;

    dbg!(&playlist_node);

    if playlist_node.base_node.expanded {
        return Ok(playlist_node);
    }

    let tracks = client
        .playlists()
        .get_playlists_items(id, 50, 0, None)
        .await?
        .data
        .items;

    let mut tracks = vec![];

    let mut current = 0;
    let mut total = 0;

    loop {
        let resp = client.playlists().get_playlists_items(id, 50, current, None).await?;

        current += resp.data.items.len();

        tracks.extend(resp.data.items);

        if total == 0 {
            total = resp.data.total;
        }

        if current >= total {
            break;
        }
    }


    for track in tracks {
        if let Some(aspotify::PlaylistItemType::Track(track)) = track.item {
            let track_id = track.id.clone().unwrap();
            let track_node = create_node_or_try_get_by_key_value(graph, &TrackNode::from(track), true, ("spotify_id", track_id)).await?;

            create_edge(graph, &track_node, &playlist_node, TrackOnPlaylist).await?;
        }
    }

    playlist_node.base_node.expanded = true;

    Ok(update_node(graph, &playlist_node).await?)
}

pub async fn expand_track_node(
    graph: &mut NeoGraph,
    client: &aspotify::Client,
    id: &str,
) -> Result<NeoNode<TrackNode>, crate::Error> {
    let mut track_node = find_by_key_value::<TrackNode, _>(graph, None, ("spotify_id", id), false).await?;

    if track_node.base_node.expanded {
        return Ok(track_node);
    }

    let track = crate::spotify::track::get_track(client, id).await?;

    let album = track.album;

    let album_node = AlbumNode::from(album);
    let album_id = album_node.base_node.spotify_id.clone();
    let album_node = create_node_or_try_get_by_key_value(graph, &album_node, true, ("spotify_id", album_id)).await?;

    create_edge(graph, &track_node, &album_node, TrackOnAlbum).await?;

    let artists = track.artists;

    for artist in artists {
        let artist_id = artist.id.clone().unwrap();
        let artist_node = create_node_or_try_get_by_key_value(graph, &ArtistNode::from(artist), true, ("spotify_id", artist_id)).await?;

        create_edge(graph, &artist_node, &track_node, ArtistOnTrack).await?;
    }

    track_node.base_node.expanded = true;

    Ok(update_node(graph, &track_node).await?)
}

#[derive(Serialize, Deserialize, Debug)]
pub enum ExpandableNode {
    Album(NeoNode<AlbumNode>), Artist(NeoNode<ArtistNode>), Playlist(NeoNode<PlaylistNode>), Track(NeoNode<TrackNode>)
}

impl ExpandableNode {
    pub async fn expand_node(&self, graph: &mut NeoGraph, client: &aspotify::Client) -> Result<Self, crate::Error> {
        use ExpandableNode::*;
        Ok(match self {
            Album(album) => Album(expand_album_node(graph, client, &album.base_node.spotify_id).await?),
            Artist(artist) => Artist(expand_artist_node(graph, client, &artist.base_node.spotify_id).await?),
            Playlist(playlist) => Playlist(expand_playlist_node(graph, client, &playlist.base_node.spotify_id).await?),
            Track(track) => Track(expand_track_node(graph, client, &track.base_node.spotify_id).await?)
        })
    }

    pub fn into_untyped_node_dto(self) -> UntypedNodeDTO {
        use ExpandableNode::*;
        match self {
            Album(album) => NodeDTO::from(album).into_untyped(),
            Artist(artist) => NodeDTO::from(artist).into_untyped(),
            Playlist(playlist) => NodeDTO::from(playlist).into_untyped(),
            Track(track) => NodeDTO::from(track).into_untyped(),
        }
    }

    pub fn is_expanded(&self) -> bool {
        use ExpandableNode::*;
        match self {
            Album(album) => album.base_node.expanded,
            Artist(artist) => artist.base_node.expanded,
            Playlist(playlist) => playlist.base_node.expanded,
            Track(track) => track.base_node.expanded,
        }
    }

    pub fn get_spotify_id(&self) -> &str {
        use ExpandableNode::*;
        match self {
            Album(album) => &album.base_node.spotify_id,
            Artist(artist) => &artist.base_node.spotify_id,
            Playlist(playlist) => &playlist.base_node.spotify_id,
            Track(track) => &track.base_node.spotify_id,
        }
    }
}

impl Node for ExpandableNode {
    fn get_labels() -> Vec<String> {
        vec!("Expandable".into())
    }

    fn get_properties(&self) -> HashMap<String, BoltType, RandomState> {
        use ExpandableNode::*;
        match self {
            Album(album) => album.get_properties(),
            Artist(artist) => artist.get_properties(),
            Playlist(playlist) => playlist.get_properties(),
            Track(track) => track.get_properties(),
        }
    }

    fn get_id(&self) -> Option<i64> {
        use ExpandableNode::*;
        Some(match self {
            Album(album) => album.id(),
            Artist(artist) => artist.id(),
            Playlist(playlist) => playlist.id(),
            Track(track) => track.id(),
        })
    }

    fn from_bolt_node(node: &neo4rs::Node) -> Self {
        let labels = node.labels();
        let label = labels.into_iter().find(|label| ["Album", "Artist", "Playlist", "Track"].contains(&&**label)).unwrap();

        match label {
            _ if label == "Album" => Self::Album(NeoNode(node.id(), node.labels(), AlbumNode::from_bolt_node(node))),
            _ if label == "Artist" => Self::Artist(NeoNode(node.id(), node.labels(), ArtistNode::from_bolt_node(node))),
            _ if label == "Playlist" => Self::Playlist(NeoNode(node.id(), node.labels(), PlaylistNode::from_bolt_node(node))),
            _ if label == "Track" => Self::Track(NeoNode(node.id(), node.labels(), TrackNode::from_bolt_node(node))),
            _ => panic!("")
        }
    }
}

pub async fn expand_node_by_id(graph: &mut NeoGraph, client: &aspotify::Client, id: &str) -> Result<ExpandableNode, crate::Error> {
    let node = query::<neo4rs::Node, _, _>(graph, "MATCH (n) WHERE n.spotify_id = $spotify_id RETURN n;", vec![("spotify_id", id)], "n").await?;
    let expandable = ExpandableNode::from_bolt_node(&node);

    if expandable.is_expanded() {
        return Ok(expandable);
    }

    Ok(expandable.expand_node(graph, client).await?)
}

pub async fn expand_random_artist_node(graph: &mut NeoGraph, client: &aspotify::Client) -> Result<ExpandableNode, crate::Error> {
    let node = find_by_key_value::<ArtistNode, _>(graph, Some("Artist".into()), ("expanded", neo4rs::BoltType::Boolean(BoltBoolean::new(false))), true).await?;
    let node = ExpandableNode::Artist(node);

    Ok(node.expand_node(graph, client).await?)
}

pub async fn expand_random_node(graph: &mut NeoGraph, client: &aspotify::Client, label: Option<String>) -> Result<ExpandableNode, crate::Error> {
    let node = find_by_key_value::<ExpandableNode, _>(graph, label, ("expanded", neo4rs::BoltType::Boolean(BoltBoolean::new(false))), true).await?;

    info!("Expanding node {}", node.0);

    Ok(node.expand_node(graph, client).await?)
}
