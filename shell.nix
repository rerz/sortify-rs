{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
    buildInputs = with pkgs; [
        autoconf
        automake
        clang
        gcc
        libtool
        pkgconfig
        libopus
        openssl
    ];
    shellHook = ''
    '';
}
