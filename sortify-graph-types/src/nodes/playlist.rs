use sortify_graph_traits::Node;
use std::collections::hash_map::RandomState;
use std::collections::HashMap;
use bolt_proto::Value;
use crate::nodes::SpotifyEntityNode;
use aspotify::Playlist;

use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct PlaylistNode {
    pub base_node: SpotifyEntityNode,
}

impl Node for PlaylistNode {
    fn get_labels() -> Vec<String> {
        vec!["Playlist".into(), "SpotifyEntity".into()]
    }

    fn get_properties(&self) -> HashMap<String, neo4rs::BoltType> {
        let mut map: HashMap<_, _> = maplit::hashmap! {};

        map.extend(self.base_node.get_properties());

        map
    }

    fn get_id(&self) -> Option<i64> {
        unimplemented!()
    }

    fn from_bolt_node(node: &neo4rs::Node) -> Self {
        Self {
            base_node: SpotifyEntityNode::from_bolt_node(node)
        }
    }
}

impl From<aspotify::Playlist> for PlaylistNode {
    fn from(playlist: aspotify::Playlist) -> Self {
        Self {
            base_node: SpotifyEntityNode::from(playlist),
        }
    }
}