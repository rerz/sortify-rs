use sortify_graph_traits::Node;
use std::collections::hash_map::RandomState;
use std::collections::HashMap;
use bolt_proto::Value;
use std::convert::{TryFrom, TryInto};
use aspotify::{Album, Artist, Playlist, Track, ArtistsAlbum};

pub mod album;
pub mod artist;
pub mod playlist;
pub mod track;

use serde::{Serialize, Deserialize};
use neo4rs::types::BoltBoolean;

#[derive(Serialize, Deserialize, Debug)]
pub struct SpotifyEntityNode {
    pub spotify_id: String,
    pub name: String,
    pub expanded: bool,
}

impl Node for SpotifyEntityNode {
    fn get_labels() -> Vec<String> {
        vec!["SpotifyEntity".into()]
    }

    fn get_properties(&self) -> HashMap<String, neo4rs::BoltType> {
        maplit::hashmap! {
            "spotify_id".into() => neo4rs::BoltType::try_from(self.spotify_id.clone()).unwrap(),
            "name".into() => neo4rs::BoltType::try_from(self.name.clone()).unwrap(),
            "expanded".into() => neo4rs::BoltType::Boolean(BoltBoolean::new(self.expanded)),
        }
    }

    fn get_id(&self) -> Option<i64> {
        unimplemented!()
    }

    fn from_bolt_node(node: &neo4rs::Node) -> Self {
        Self {
            spotify_id: node.get("spotify_id").unwrap(),
            name: node.get("name").unwrap(),
            expanded: node.get("expanded").unwrap(),
        }
    }
}

impl From<aspotify::Album> for SpotifyEntityNode {
    fn from(album: aspotify::Album) -> Self {
        Self {
            spotify_id: album.id,
            name: album.name,
            expanded: false,
        }
    }
}

impl From<aspotify::ArtistsAlbum> for SpotifyEntityNode {
    fn from(album: aspotify::ArtistsAlbum) -> Self {
        Self {
            spotify_id: album.id,
            name: album.name,
            expanded: false,
        }
    }
}

impl From<aspotify::Artist> for SpotifyEntityNode {
    fn from(artist: aspotify::Artist) -> Self {
        Self {
            spotify_id: artist.id,
            name: artist.name,
            expanded: false,
        }
    }
}

impl From<aspotify::Playlist> for SpotifyEntityNode {
    fn from(playlist: aspotify::Playlist) -> Self {
        Self {
            spotify_id: playlist.id,
            name: playlist.name,
            expanded: false,
        }
    }
}

impl From<aspotify::Track> for SpotifyEntityNode {
    fn from(track: aspotify::Track) -> Self {
        Self {
            spotify_id: track.id.expect("Track does not have an id"),
            name: track.name,
            expanded: false,
        }
    }
}