use crate::nodes::SpotifyEntityNode;
use sortify_graph_traits::Node;
use std::collections::hash_map::RandomState;
use std::collections::HashMap;
use std::convert::{TryFrom, TryInto};

use aspotify::{AlbumSimplified, ArtistsAlbum};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct AlbumNode {
    //#[base_node]
    pub base_node: SpotifyEntityNode,
}

impl Node for AlbumNode {
    fn get_labels() -> Vec<String> {
        vec!["Album".into(), "SpotifyEntity".into()]
    }

    fn get_properties(&self) -> HashMap<String, neo4rs::BoltType> {
        let mut map: HashMap<_, _> = maplit::hashmap! {};

        map.extend(self.base_node.get_properties());

        map
    }

    fn get_id(&self) -> Option<i64> {
        unimplemented!()
    }

    fn from_bolt_node(node: &neo4rs::Node) -> Self {
        Self {
            base_node: SpotifyEntityNode::from_bolt_node(node),
        }
    }
}

impl From<aspotify::Album> for AlbumNode {
    fn from(album: aspotify::Album) -> Self {
        Self {
            base_node: SpotifyEntityNode::from(album),
        }
    }
}

impl From<aspotify::ArtistsAlbum> for AlbumNode {
    fn from(album: ArtistsAlbum) -> Self {
        Self {
            base_node: SpotifyEntityNode::from(album),
        }
    }
}

impl From<aspotify::AlbumSimplified> for AlbumNode {
    fn from(album: AlbumSimplified) -> Self {
        Self {
            base_node: SpotifyEntityNode {
                name: album.name,
                spotify_id: album.id.unwrap(),
                expanded: false,
            },
        }
    }
}
