use sortify_graph_traits::Node;
use std::collections::hash_map::RandomState;
use std::collections::HashMap;
use bolt_proto::Value;
use crate::nodes::SpotifyEntityNode;

use serde::{Serialize, Deserialize};
use aspotify::TrackSimplified;

#[derive(Serialize, Deserialize, Debug)]
pub struct TrackNode {
    pub base_node: SpotifyEntityNode,
}

impl Node for TrackNode {
    fn get_labels() -> Vec<String> {
        vec!["Track".into(), "SpotifyEntity".into()]
    }

    fn get_properties(&self) -> HashMap<String, neo4rs::BoltType> {
        let mut map: HashMap<_, _> = maplit::hashmap! {};

        map.extend(self.base_node.get_properties());

        map
    }

    fn get_id(&self) -> Option<i64> {
        unimplemented!()
    }

    fn from_bolt_node(node: &neo4rs::Node) -> Self {
        Self {
            base_node: SpotifyEntityNode::from_bolt_node(node)
        }
    }
}

impl From<aspotify::Track> for TrackNode {
    fn from(track: aspotify::Track) -> Self {
        Self {
            base_node: SpotifyEntityNode::from(track),
        }
    }
}

impl From<aspotify::TrackSimplified> for TrackNode {
    fn from(track: aspotify::TrackSimplified) -> Self {
        Self {
            base_node: SpotifyEntityNode {
                spotify_id: track.id.unwrap(),
                name: track.name,
                expanded: false,
            }
        }
    }
}