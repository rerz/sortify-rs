use crate::nodes::album::AlbumNode;
use crate::nodes::artist::ArtistNode;
use crate::nodes::playlist::PlaylistNode;
use crate::nodes::track::TrackNode;
use sortify_graph_traits::Edge;

pub struct ArtistOnTrack;

impl Edge for ArtistOnTrack {
    type Source = ArtistNode;
    type Target = TrackNode;

    fn get_labels() -> Vec<String> {
        vec!["ArtistOnTrack".into()]
    }
}
