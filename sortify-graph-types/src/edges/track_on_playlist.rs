use crate::nodes::album::AlbumNode;
use crate::nodes::artist::ArtistNode;
use crate::nodes::playlist::PlaylistNode;
use crate::nodes::track::TrackNode;
use sortify_graph_traits::Edge;

pub struct TrackOnPlaylist;

impl Edge for TrackOnPlaylist {
    type Source = TrackNode;
    type Target = PlaylistNode;

    fn get_labels() -> Vec<String> {
        vec!["TrackOnPlaylist".into()]
    }
}
