use sortify_graph_traits::Edge;
use crate::nodes::artist::ArtistNode;
use crate::nodes::album::AlbumNode;

pub struct ArtistOnAlbum;

impl Edge for ArtistOnAlbum {
    type Source = ArtistNode;
    type Target = AlbumNode;

    fn get_labels() -> Vec<String> {
        vec!("ArtistOnAlbum".into())
    }
}