use crate::nodes::album::AlbumNode;
use crate::nodes::artist::ArtistNode;
use crate::nodes::playlist::PlaylistNode;
use crate::nodes::track::TrackNode;
use sortify_graph_traits::Edge;

pub struct TrackOnAlbum;

impl Edge for TrackOnAlbum {
    type Source = TrackNode;
    type Target = AlbumNode;

    fn get_labels() -> Vec<String> {
        vec!["TrackOnAlbum".into()]
    }
}
