use crate::nodes::album::AlbumNode;
use crate::nodes::artist::ArtistNode;
use crate::nodes::playlist::PlaylistNode;
use crate::nodes::track::TrackNode;
use sortify_graph_traits::Edge;

pub struct ArtistRelatedTo;

impl Edge for ArtistRelatedTo {
    type Source = ArtistNode;
    type Target = ArtistNode;

    fn get_labels() -> Vec<String> {
        vec!["ArtistRelatedTo".into()]
    }
}
