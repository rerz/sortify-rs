import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';

import ForceGraph, {ForceGraph3DGenericInstance, ForceGraph3DInstance} from '3d-force-graph';
import {HttpClient} from "@angular/common/http";
import SpriteText from "three-spritetext";

interface NodeDTO {
  id: number,
  labels: Array<string>,
  node: { name: string, weight: number, },
  community: number,
}

interface SubgraphDTO {
  nodes: Array<NodeDTO>,
  edges: Array<[number, number]>,
}

@Component({
  selector: 'sortify-frontend-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'sortify-graph';

  @ViewChild("graph") graphElement: ElementRef<HTMLDivElement>;
  @ViewChild("queryTextArea") queryTextArea: ElementRef<HTMLTextAreaElement>;

  public query: string;

  private graph: ForceGraph3DInstance;

  constructor(private readonly http: HttpClient) {
    this.graph = ForceGraph();
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.graph(this.graphElement.nativeElement)
      .linkWidth(2)
      .width(1280)
      .height(720)
      .enableNodeDrag(false)
      .nodeVal(node => (node as any).node.weight)
      .nodeAutoColorBy(node => (node as any).community)
      .nodeLabel(node => (node as any).node.name)
      .graphData({ nodes: [], links: [] });

    const force = this.graph.d3Force('link') as any;
    force
      .distance(d => 250);
  }

  executeQuery() {
    this.http.get(`http://127.0.0.1:8080/graph?query=${encodeURIComponent(this.query)}`).subscribe((resp: SubgraphDTO) => {
      console.log();

      const nodeIDs = resp.nodes.map(node => node.id);
      const edges = resp.edges.filter(([s, t]) => nodeIDs.includes(s) && nodeIDs.includes(t));

      this.graph.graphData({
        nodes: resp.nodes.map(node => ({ id: node.id, ...node })),
        links: edges.map(([source, target]) => ({ source, target }))
      });
    });
  }

}
