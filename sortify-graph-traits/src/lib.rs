use std::collections::HashMap;

pub trait Node {
    fn get_labels() -> Vec<String>;

    fn get_properties(&self) -> HashMap<String, neo4rs::BoltType>;

    fn get_id(&self) -> Option<i64>;

    fn from_bolt_node(node: &neo4rs::Node) -> Self;
}

// Also impl From<bolt_proto::value::Node> for impl Node;

pub trait Edge {
    type Source: Node;
    type Target: Node;

    fn get_labels() -> Vec<String>;
}

pub trait Graph {
}

pub trait Expand: Node {
    fn expand(&mut self);
}

pub trait SpotifyEntity: Node {

}